# Cosmos <> Ethereum bridge's web application

This repo contains the web application, in Vue Js, for the Cosmos <> Ethereum bridge PoC

## Project setup
```
npm install
```

### Compiles and launch the app
```
npm run serve
```
