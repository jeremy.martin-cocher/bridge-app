const Web3 = require("web3")
//change it when zone is operational
const zoneId = 'cosmoshub-4'

var KeplrOk = {"status" : 200, "wallet" : "keplr", "sendingAddress": null}
var KeplrError = {"status" : 401, "wallet" : "keplr", "sendingAddress": null}
var MetamaskOk = {"status" : 200, "wallet" : "metamask", "sendingAddress": null}
var MetamaskError = {"status" : 401, "wallet" : "metamask", "sendingAddress": null}

export async function connectToWeb3(window,fromCosmosToEth){
  let web3;
  if(fromCosmosToEth){
    if(window.keplr){
      console.log("Keplr wallet detected.")
      KeplrOk.sendingAddress = null;
      web3 = new Web3(window.keplr)
      try{
        await window.keplr.enable('cosmoshub-4')
        KeplrOk.sendingAddress = await getSendingAddress(window,fromCosmosToEth)
        return KeplrOk;
      }catch(error){
        return KeplrError
      }
    }
    else{
      console.log("no cosmos wallet detected.")
    }
  }
  //ETH to COSMOS ==> connect metamask wallet 
  else{
    if(window.ethereum){
      console.log("metamask installed")
      MetamaskOk.sendingAddress = null;
      web3 = new Web3(window.ethereum)
      try{
        await window.ethereum.enable()
        MetamaskOk.sendingAddress = await getSendingAddress(window,fromCosmosToEth)
        return MetamaskOk
      }catch(error){
        return MetamaskError
      }
    }
    else{
      console.log("no eth wallet detected.")
    }
  }
  //to avoid web3 is assigned but never used error
  console.log(web3)
}

export async function getSendingAddress(window,fromCosmosToEth){
  if(fromCosmosToEth){
    // let web3 = new Web3(window.keplr)
    try{
      const account = await window.keplr.getKey(zoneId)
      return account.bech32Address
    }
    catch(error){
      console.log(error)
      return null
    }
  }
  else{
    let web3 = new Web3(window.ethereum)
    try{
      console.log("trying to get eth accounts")
      const accounts = await web3.eth.getAccounts()
      return accounts[0]
    } catch(error){
      console.log(error)
      return null
    }
  }
}