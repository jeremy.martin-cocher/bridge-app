import {createStore} from 'vuex'

const store = createStore({
    state:{
        fromCosmosToEth: true,
        receivingAddress: null,
        ethSendingAddress: null,
        cosmosSendingAddress: null,
        isAddressValid: true,
        isKeplrWalletConnected: false,
        isMetamaskWalletConnected: false,
        amountToSend: 0
    },
    getters:{
        
    },
    mutations:{
        changeSwapDirection(state){
            state.fromCosmosToEth = !state.fromCosmosToEth;
            //if we change the swap direction the user have to connect with another wallet
            state.isWalletConnected = false
        },
        setReceivingAddress(state, newAddress){
            state.receivingAddress = newAddress;
        },
        setIsMetamaskWalletConnected(state, newValue){
            state.isMetamaskWalletConnected = newValue;
        },
        setIsKeplrWalletConnected(state, newValue){
            state.isKeplrWalletConnected = newValue;
        },
        setAmountToSend(state, newValue){
            state.amountToSend = newValue;
        },
        setIsAddressValid(state, newValue){
            state.isAddressValid = newValue;
        },
        setCosmosSendingAddress(state, newValue){
            state.cosmosSendingAddress = newValue;
        },
        setEthSendingAddress(state, newValue){
            state.ethSendingAddress = newValue;
        }
    },
    actions:{

    },
    modules:{

    }
})

export default store;