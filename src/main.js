import { createApp } from 'vue'
import App from './App.vue'
import store from './store'

//primevue components that I am goin to use
import PrimeVue from 'primevue/config'
import Button from 'primevue/button'
import InputNumber from 'primevue/inputnumber'
import Dropdown from 'primevue/dropdown'
import InputText from 'primevue/inputtext'
import ToggleButton from 'primevue/togglebutton'
import SelectButton from 'primevue/selectbutton'
import ToastService from 'primevue/toastservice'
import Toast from 'primevue/toast'
import Menubar from 'primevue/menubar'

//our css files
import 'primevue/resources/themes/vela-purple/theme.css'       //theme
import 'primevue/resources/primevue.min.css'                 //core css
import 'primeicons/primeicons.css'                           //icons
import 'primeflex/primeflex.css'

const app = createApp(App)
app.config.globalProperties._depsLoaded = true
//install primevue
app.use(PrimeVue)
app.use(store)
app.use(ToastService)
//add components
app.component('Button', Button)
app.component('InputNumber',InputNumber)
app.component('Dropdown',Dropdown)
app.component('InputText',InputText)
app.component('ToggleButton',ToggleButton)
app.component('SelectButton',SelectButton)
app.component('Toast',Toast)
app.component('Menubar',Menubar)
//mount app
app.mount('#app');
